package controllers;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
public class ErrorsController implements org.springframework.boot.autoconfigure.web.ErrorController{

	@Override
	public String getErrorPath() {
		return "homepage.jsp";
	}
	
	@RequestMapping("/")
	public String slashMapping() {
		return "homepage.jsp";
	}
	@RequestMapping("/error")
	public String errorMapping() {
		return "homepage.jsp";
	}
}
